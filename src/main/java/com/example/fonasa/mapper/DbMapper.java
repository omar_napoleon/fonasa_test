/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.fonasa.mapper;

import com.example.fonasa.model.Consulta;
import com.example.fonasa.model.Paciente;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 *
 * @author usuario
 */
@Mapper
public interface DbMapper {

    @Select({"select p.NOMBRE, p.EDAD, n.RELACION_PESO_ESTATURA, j.FUMADOR,",
        "j.ANNOS, a.TIENE_DIETA ",
        "from PACIENTE p",
        "LEFT JOIN PACIENTE_NINNO n on n.ID_PACIENTE  = p.ID",
        "LEFT JOIN PACIENTE_JOVEN j on j.ID_PACIENTE  = p.ID",
        "LEFT JOIN PACIENTE_ANCIANO a on a.ID_PACIENTE  = p.ID",
        "where p.NRO_HISTORIA_CLINICA = #{id}"})
    @Results({
        @Result(property = "edad", column = "EDAD")
        ,
    @Result(property = "nombre", column = "NOMBRE")
        ,
    @Result(property = "relacionPesoEstatura", column = "RELACION_PESO_ESTATURA")
        ,
    @Result(property = "fumador", column = "FUMADOR")
        ,
    @Result(property = "annosFumador", column = "ANNOS")
        ,
    @Result(property = "tieneDieta", column = "TIENE_DIETA"),})
    public Paciente findPacienteByNro(Integer id);

    @Select({"select DISTINCT p.NRO_HISTORIA_CLINICA, p.NOMBRE, p.EDAD,n.RELACION_PESO_ESTATURA, j.FUMADOR,",
        "j.ANNOS, a.TIENE_DIETA, h.NOMBRE as H_NOMBRE,",
        "(CASE WHEN p.EDAD >= 41 AND p.EDAD <=59 THEN ((p.EDAD*1.0*((p.EDAD*1.0/30)+3)/100) + 5.3)",
        " WHEN p.EDAD >= 60 AND p.EDAD <=100 AND a.TIENE_DIETA is true AND ((p.EDAD*1.0*((p.EDAD*1.0/20) +4)/100) + 5.3) > #{riesgo} THEN ((p.EDAD*1.0*((p.EDAD*1.0/20) +4)/100) + 5.3)",
        " WHEN p.EDAD >= 60 AND p.EDAD <=100 AND a.TIENE_DIETA is false AND ((p.EDAD*1.0*((p.EDAD*1.0/30) +3)/100) + 5.3) > #{riesgo} THEN ((p.EDAD*1.0*((p.EDAD*1.0/30) +3)/100) + 5.3)",
        " WHEN p.EDAD >= 15 AND p.EDAD <=40 AND j.FUMADOR is false AND (p.EDAD*1.0*2/100) > #{riesgo} THEN (p.EDAD*1.0*2/100)",
        " WHEN p.EDAD >= 15 AND p.EDAD <=40 AND j.FUMADOR is true AND (p.EDAD*1.0*((j.ANNOS*1.0/4) +2)/100) > #{riesgo} THEN (p.EDAD*1.0*((j.ANNOS*1.0/4) +2)/100)",
        " WHEN p.EDAD >= 1  AND p.EDAD <=15 AND (p.EDAD*1.0*n.RELACION_PESO_ESTATURA/100) > #{riesgo} THEN (p.EDAD*1.0*n.RELACION_PESO_ESTATURA/100) END) AS RIESGO",
        "from PACIENTE p",
        "LEFT JOIN PACIENTE_ANCIANO a on a.ID_PACIENTE  = p.ID",
        "LEFT JOIN PACIENTE_JOVEN j on j.ID_PACIENTE  = p.ID",
        "LEFT JOIN PACIENTE_NINNO n on n.ID_PACIENTE  = p.ID",
        "LEFT JOIN HOSPITAL h on h.ID  = p.ID_HOSPITAL",
        "WHERE ",
        "(CASE WHEN p.EDAD >= 41 AND p.EDAD <=59 THEN ((p.EDAD*1.0*((p.EDAD*1.0/30)+3)/100) + 5.3)",
        " WHEN p.EDAD >= 60 AND p.EDAD <=100 AND a.TIENE_DIETA is true AND ((p.EDAD*1.0*((p.EDAD*1.0/20) +4)/100) + 5.3) > #{riesgo} THEN ((p.EDAD*1.0*((p.EDAD*1.0/20) +4)/100) + 5.3)",
        " WHEN p.EDAD >= 60 AND p.EDAD <=100 AND a.TIENE_DIETA is false AND ((p.EDAD*1.0*((p.EDAD*1.0/30) +3)/100) + 5.3) > #{riesgo} THEN ((p.EDAD*1.0*((p.EDAD*1.0/30) +3)/100) + 5.3)",
        " WHEN p.EDAD >= 15 AND p.EDAD <=40 AND j.FUMADOR is false AND (p.EDAD*1.0*2/100) > #{riesgo} THEN (p.EDAD*1.0*2/100)",
        " WHEN p.EDAD >= 15 AND p.EDAD <=40 AND j.FUMADOR is true AND (p.EDAD*1.0*((j.ANNOS*1.0/4) +2)/100) > #{riesgo} THEN (p.EDAD*1.0*((j.ANNOS*1.0/4) +2)/100)",
        " WHEN p.EDAD >= 1  AND p.EDAD <=15 AND (p.EDAD*1.0 * n.RELACION_PESO_ESTATURA/100) > #{riesgo} THEN (p.EDAD*1.0*n.RELACION_PESO_ESTATURA/100) END) > #{riesgo}",
        " ORDER BY RIESGO DESC"})
    @Results({
        @Result(property = "edad", column = "EDAD")
        ,
    @Result(property = "nombre", column = "NOMBRE")
        ,
    @Result(property = "relacionPesoEstatura", column = "RELACION_PESO_ESTATURA")
        ,
    @Result(property = "fumador", column = "FUMADOR")
        ,
    @Result(property = "nroHistoriaClinica", column = "NRO_HISTORIA_CLINICA")
        ,
    @Result(property = "annosFumador", column = "ANNOS")
        ,
    @Result(property = "tieneDieta", column = "TIENE_DIETA")
        ,
    @Result(property = "nombreHospital", column = "H_NOMBRE")
        ,
    @Result(property = "riesgo", column = "RIESGO")})
    public List<Paciente> findPacienteNinnoByRiesgo(Double riesgo);

    @Select({"select p.NRO_HISTORIA_CLINICA, p.NOMBRE, p.EDAD,",
        "j.ANNOS, (p.EDAD*1.0*((j.ANNOS*1.0/4) +2)/100) AS RIESGO",
        "from PACIENTE p , PACIENTE_JOVEN j, HOSPITAL h",
        " WHERE j.ID_PACIENTE  = p.ID AND h.ID  = p.ID_HOSPITAL AND j.FUMADOR = true ORDER BY RIESGO DESC"
    })
    @Results({
        @Result(property = "edad", column = "EDAD")
        ,
    @Result(property = "nombre", column = "NOMBRE")
        ,
    @Result(property = "nroHistoriaClinica", column = "NRO_HISTORIA_CLINICA")
        ,
    @Result(property = "annosFumador", column = "ANNOS")
        ,
    @Result(property = "nombreHospital", column = "H_NOMBRE")
        ,
    @Result(property = "riesgo", column = "RIESGO")})
    public List<Paciente> findPacientesFumadoresMayorRiesgo();

    @Select({"select c.CANT_PACIENTES as MAX_CANTIDAD, c.NOMBRE_ESPECIALISTA, c.TIPO_CONSULTA, h.NOMBRE as H_NOMBRE ",
        "FROM CONSULTA c, HOSPITAL h WHERE h.ID  = c.ID_HOSPITAL",
        "AND c.CANT_PACIENTES = (select max(CANT_PACIENTES) from CONSULTA) GROUP BY c.ID "
    })
    @Results({
        @Result(property = "cantidadPacientes", column = "MAX_CANTIDAD")
        ,
    @Result(property = "nombreEspecialista", column = "NOMBRE_ESPECIALISTA")
        ,
    @Result(property = "tipoConsulta", column = "TIPO_CONSULTA")
        ,
    @Result(property = "nombreHospital", column = "H_NOMBRE")
    })
    public List<Consulta> findConsultasMayorCantPacientes();
    
    @Update({"update CONSULTA set ESTADO = 'DESOCUPADO', CANT_PACIENTES = CANT_PACIENTES +1 ",
    "WHERE ESTADO = 'OCUPADO' AND ID_HOSPITAL = 1"})
    public Integer updateEstadoConsulta();
    

}
