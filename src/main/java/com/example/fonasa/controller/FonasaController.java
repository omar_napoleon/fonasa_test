/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.fonasa.controller;

import com.example.fonasa.model.Consulta;
import com.example.fonasa.model.Paciente;
import com.example.fonasa.service.FonasaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author usuario
 */
@Api(value = "Gestión de consultas Fonasa Rest API")
@RestController
public class FonasaController {

    @Autowired
    private FonasaService fonasaService;

    /**
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "Lista de pacientes con mayor riesgo", response = Paciente.class, responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Lista recuperada exitosamente", response = Paciente.class, responseContainer = "List")
        ,
    @ApiResponse(code = 404, message = "No se encuentra el recurso" )
        ,
    @ApiResponse(code = 500, message = "Error interno")
    })
    @GetMapping(value = "/pacientes/{id}", produces = {"application/json"})
    public ResponseEntity<List<Paciente>> getPacientesByNroHistoria(
            @ApiParam(value = "Número de historia del paciente", required = true)
            @Valid @PathVariable("id") @NumberFormat Integer id) {
        List<Paciente> pacientes = new ArrayList<>();
        try {
            pacientes = fonasaService.listarPacientesMayorRiesgo(id);
            if (pacientes == null || pacientes.isEmpty()) {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }

            return new ResponseEntity<>(pacientes, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     *
     * @return
     */
    @ApiOperation(value = "Lista de pacientes fumadores con mayor riesgo", response = Paciente.class, responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Lista recuperada exitosamente", response = Paciente.class, responseContainer = "List")
        ,
    @ApiResponse(code = 404, message = "No se encuentra el recurso")
        ,
    @ApiResponse(code = 500, message = "Error interno")
    })
    @GetMapping(value = "/pacientes/fumadores", produces = {"application/json"})
    public ResponseEntity<List<Paciente>> getFumadores() {
        List<Paciente> pacientes = new ArrayList<>();
        try {
            pacientes = fonasaService.listarPacientesFumadores();
            if (pacientes == null) {
                return new ResponseEntity<>(pacientes, HttpStatus.NOT_FOUND);
            }

            return new ResponseEntity<>(pacientes, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     *
     * @return
     */
    @ApiOperation(value = "Lista de consultas con mayor cantidad de pacientes atendidos", response = Consulta.class, responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Lista recuperada exitosamente", response = Consulta.class, responseContainer = "List")
        ,
    @ApiResponse(code = 404, message = "No se encuentra el recurso")
        ,
    @ApiResponse(code = 500, message = "Error interno")
    })
    @GetMapping(value = "/consultas/mayorCantidad", produces = {"application/json"})
    public ResponseEntity<List<Consulta>> getConsultasMasPacientes() {
        List<Consulta> consulta = new ArrayList<>();
        try {
            consulta = fonasaService.listarConsultasMayorCantidad();
            if (consulta == null) {
                return new ResponseEntity<>(consulta, HttpStatus.NOT_FOUND);
            }

            return new ResponseEntity<>(consulta, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     *
     * @return
     */
    @ApiOperation(value = "Actualización que libera las consultas ocupadas de un hospital", response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Actualización realizada exitosamente", response = String.class)
        ,
    @ApiResponse(code = 404, message = "No se encuentra el recurso")
        ,
    @ApiResponse(code = 500, message = "Error interno")
    })
    @RequestMapping(path = "/consultas/liberar", method = RequestMethod.PATCH)
    public ResponseEntity<String> updateConsultas() {
        String result = null;
        try {
            result = fonasaService.updateEstadoConsulta();
            if (result == null) {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }

            return new ResponseEntity<>(null, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
