/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.fonasa.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;

/**
 *
 * @author usuario
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "Atributos del paciente")
public class Paciente implements Serializable {
    
    private Integer id;

    @ApiModelProperty(notes = "Nombre del paciente")
    private String nombre;

    @ApiModelProperty(notes = "Número de historia clínica")
    private Integer nroHistoriaClinica;

    @ApiModelProperty(notes = "Edad")
    private Integer edad;

    @ApiModelProperty(notes = "Prioridad")
    private Double prioridad;
    
    @ApiModelProperty(notes = "Riesgo")
    private Double riesgo;
    
    @ApiModelProperty(notes = "Es fumador")
    private Boolean fumador;
    
    @ApiModelProperty(notes = "Años de fumador")
    private Integer annosFumador;
    
    private Integer relacionPesoEstatura;
    
    private Boolean tieneDieta;
    
    @ApiModelProperty(notes = "Nombre del hospital donde fue registrado")
    private String nombreHospital;

    public String getNombreHospital() {
        return nombreHospital;
    }

    public void setNombreHospital(String nombreHospital) {
        this.nombreHospital = nombreHospital;
    }

    public Double getRiesgo() {
        return riesgo;
    }

    public void setRiesgo(Double riesgo) {
        this.riesgo = riesgo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getNroHistoriaClinica() {
        return nroHistoriaClinica;
    }

    public void setNroHistoriaClinica(Integer nroHistoriaClinica) {
        this.nroHistoriaClinica = nroHistoriaClinica;
    }

    public Double getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(Double prioridad) {
        this.prioridad = prioridad;
    }

    public Boolean getFumador() {
        return fumador;
    }

    public void setFumador(Boolean fumador) {
        this.fumador = fumador;
    }

    public Integer getAnnosFumador() {
        return annosFumador;
    }

    public void setAnnosFumador(Integer annosFumador) {
        this.annosFumador = annosFumador;
    }

    public Integer getRelacionPesoEstatura() {
        return relacionPesoEstatura;
    }

    public void setRelacionPesoEstatura(Integer relacionPesoEstatura) {
        this.relacionPesoEstatura = relacionPesoEstatura;
    }

    public Boolean getTieneDieta() {
        return tieneDieta;
    }

    public void setTieneDieta(Boolean tieneDieta) {
        this.tieneDieta = tieneDieta;
    }
    
}
