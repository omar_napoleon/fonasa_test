/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.fonasa.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;

/**
 *
 * @author usuario
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "Atributos de la consulta")
public class Consulta implements Serializable {

    @ApiModelProperty(notes = "Nombre del hospital al que pertence la consulta")
    private String nombreHospital;

    @ApiModelProperty(notes = "Nombre del Especialista de la Consulta")
    private String nombreEspecialista;

    @ApiModelProperty(notes = "Cantidad de pacientes")
    private String cantidadPacientes;

    @ApiModelProperty(notes = "Tipo de Consulta")
    private String tipoConsulta;

    public String getNombreHospital() {
        return nombreHospital;
    }

    public void setNombreHospital(String nombreHospital) {
        this.nombreHospital = nombreHospital;
    }

    public String getNombreEspecialista() {
        return nombreEspecialista;
    }

    public void setNombreEspecialista(String nombreEspecialista) {
        this.nombreEspecialista = nombreEspecialista;
    }

    public String getCantidadPacientes() {
        return cantidadPacientes;
    }

    public void setCantidadPacientes(String cantidadPacientes) {
        this.cantidadPacientes = cantidadPacientes;
    }

    public String getTipoConsulta() {
        return tipoConsulta;
    }

    public void setTipoConsulta(String tipoConsulta) {
        this.tipoConsulta = tipoConsulta;
    }

}
