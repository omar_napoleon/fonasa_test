/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.fonasa.service;

import com.example.fonasa.model.Consulta;
import com.example.fonasa.model.Paciente;
import java.util.List;

/**
 *
 * @author usuario
 */
public interface FonasaService {

    public List<Paciente> listarPacientesMayorRiesgo(Integer id) throws Exception;

    public List<Paciente> listarPacientesFumadores() throws Exception;
    
    public List<Consulta> listarConsultasMayorCantidad() throws Exception;
    
    public String updateEstadoConsulta()throws Exception;
}
