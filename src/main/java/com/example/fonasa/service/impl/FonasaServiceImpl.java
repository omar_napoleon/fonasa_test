/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.fonasa.service.impl;

import com.example.fonasa.mapper.DbMapper;
import com.example.fonasa.model.Consulta;
import com.example.fonasa.model.Paciente;
import com.example.fonasa.service.FonasaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author usuario
 */
@Service
public class FonasaServiceImpl implements FonasaService {

    @Autowired
    private DbMapper dbMapper;

    /**
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Override
    public List<Paciente> listarPacientesMayorRiesgo(Integer id) throws Exception {

        Paciente paciente = dbMapper.findPacienteByNro(id);

        if (paciente != null) {
            if (paciente.getTieneDieta() != null) {
                paciente.setPrioridad(paciente.getEdad() >= 60 && paciente.getEdad() <= 100 
                        && paciente.getTieneDieta()
                        ? Double.valueOf(paciente.getEdad()*1.0/ 20 + 4)
                        : Double.valueOf(paciente.getEdad()*1.0/ 30 + 3));
                paciente.setRiesgo(Double.valueOf(paciente.getEdad()*paciente.getPrioridad()*1.0/100) +5.3);
            } else if (paciente.getFumador() != null) {
                paciente.setPrioridad(paciente.getFumador()
                        ? Double.valueOf(paciente.getAnnosFumador()*1.0/ 4 + 2)
                        : Double.valueOf(2));
                paciente.setRiesgo(Double.valueOf(paciente.getEdad()*paciente.getPrioridad()*1.0/100));
            } else if (paciente.getRelacionPesoEstatura() != null) {
                paciente.setPrioridad(Double.valueOf(paciente.getRelacionPesoEstatura()*1.0));
                paciente.setRiesgo(Double.valueOf(paciente.getEdad()*paciente.getPrioridad()*1.0/100));
            }
            return dbMapper.findPacienteNinnoByRiesgo(paciente.getRiesgo());
        }else return null;
    }
    
    /**
     *
     * @return
     * @throws Exception
     */
    @Override
    public List<Paciente> listarPacientesFumadores() throws Exception {

        return dbMapper.findPacientesFumadoresMayorRiesgo();
    }

    /**
     *
     * @return
     * @throws Exception
     */
    @Override
    public List<Consulta> listarConsultasMayorCantidad() throws Exception {

        return dbMapper.findConsultasMayorCantPacientes();
    }
    
    /**
     *
     * @return
     * @throws Exception
     */
    @Override
    public String updateEstadoConsulta()throws Exception {

        return dbMapper.updateEstadoConsulta() < 1 ? null: "OK";
    }
}
