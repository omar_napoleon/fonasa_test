# Fonasa_Test

Prueba de cencosud de fonasa test, donde se exponen los metodo de la API REST FONASA. se uso como lenguaje de desarrollo Java con el framework Spring Boot y como base de datos H2(EL script de la estructura y los datos se encuentra dentro del código fuente en una archivo llamado data.sql). Al levantar el proyecto localmente, se crea y se inicializa la bd con datos de prueba automáticamene.

# SWAGGER (Documentación de la API)
API REST FONASA cuenta con Swagger como medio de documentación relacionada al funcionamiento y descripción de los métodos expuestos por la API, es decir, muestra todo lo necesario para poder consumir dicha API.
Para acceder al swagger de la API, se debe desplegar el micro servicio de forma local. Luego abrir una ventana con el navegador de su preferencia con la siguiente URL:
http://localhost:8080/swagger-ui.html#/fonasa-controller

## Métodos expuestos
Los métodos con los que cuenta la API son los siguientes:
```
http://localhost:8080/pacientes/{id}
``` 
Lista de pacientes con mayor riesgo

```
http://localhost:8080/pacientes/fumadores
``` 
Lista de pacientes fumadores con mayor riesgo

```
http://localhost:8080/consultas/mayorCantidad
``` 
Lista de consultas con mayor cantidad de pacientes atendidos

```
http://localhost:8080/consultas/liberar
``` 
Actualización que libera las consultas ocupadas de un hospital